# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.2.0] - 2022-11-10
### Changed
- Password validation now requires the old and new passwords to be dissimilar.
- Stopped parsing values as timestamps which aren't timestamps resulting in less ParseException errors in the log.

### Fixed
- Removing properties from an entity did not work for form keys (URN with `:` replaced by `__`).

### Security
- Added HTTP-Strict-Transport-Security Header.
- Added X-Frame-Options Header to prevent clickjacking.

## [3.1.5] - 2022-04-01
### Security
- Updated PostgreSQL JDBC Driver (CVE-2022-21724)

## [3.1.4] - 2022-01-11
### Security
- Updated Log4j (CVE-2021-44832)

## [3.1.3] - 2021-12-18
### Security
- Updated Log4j (CVE-2021-45105)

## [3.1.2] - 2021-12-15
### Security
- Updated Log4j (CVE-2021-45046)

## [3.1.1] - 2021-12-10
### Security
- Updated Log4j to fix potential remote code execution vulnerability.

## [3.1.0] - 2021-05-18
### Changed
- Replaced ```PasswordComplexityValidator``` to use Passay (https://www.passay.org/).
- Improved error reporting and logging.
- Upgraded PostgreSQL JDBC driver to version 42.

### Removed
- Generic CORS header removed.

## [3.0.0] - 2020-05-29
### Changed
- EDC now requires Java 8.
- Creation of callback address for Mainzelliste now uses ```x-forwarded-port``` request header if set.

## [2.0.1] - 2019-03-21
### Changed
- updated common-http dependency to 5.0.1

## [2.0.0] - 2019-01-16
### Added
- Re-init of cases can be forced to reload data from database.
- config object to abstract session scoped and view scoped beans
- unregister jdbc driver in servlet destroyed eventhandler
- method to execute a count(*) on a resourcequery
- Keycloak support

### Changed
- EDC now interprets http headers "x-forwarded-proto" and "x-forwarded-host" to properly work behind a reverse proxy.
- Clean code up
- EDC now interprets http headers "x-forwarded-proto" and "x-forwarded-host" to properly work behind a reverse proxy.
- config object to abstract session scoped and view scoped beans
- unregister jdbc driver in servlet destroyed eventhandler
- method to execute a count(*) on a resourcequery
- Upgrade to jersey 2
- Update common-http to latest version 5.0.0
- Update auth-dto to latest version 2.4.0
- Update samply.store to latest version 1.4.0

### Depreacated
- load entity methods without passing a config object

### Removed
- postconstruct annotations (has to be handled in implementing classes)
- several redundancies and unused classes

## [1.4.0] - 2017-08-29
### Added
- Possibility to copy the entire previous episode's values from all longitudinal forms.

### Changed
- Improved loading times by using a caching mechanism.
- Updated dependencies of several libraries to their latest versions.

### Fixed
- Fixed several i18n-related issues.
