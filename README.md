# Samply.EDC

Samply.EDC is the base library used for Samply.EDC.OSSE providing necessary classes to build own registry application.

# Usage

Use it as dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>edc</artifactId>
    <version>${version}</version>
</dependency>
```
