/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.listener;

import de.samply.edc.utils.Utils;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

/**
 * The listener interface for receiving startup events. The class that is
 * interested in processing a startup event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addStartupListener</code> method. When
 * the startup event occurs, that object's appropriate
 * method is invoked.
 */
public class StartupListener implements javax.servlet.ServletContextListener {

  public static final String INIT_PARAM_PROJECTNAME = "de.samply.projectName";
  public static final String CONTEXT_LOOKUP = "java:comp/env";
  /** The project name. */
  private static String projectName;

  /** The context. */
  private static ServletContext context;

  /**
   * Gets the servlet context.
   *
   * @return the servlet context
   */
  public static ServletContext getServletContext() {
    return context;
  }

  /**
   * Gets the project name.
   *
   * @return the project name
   */
  public static String getProjectName() {
    return projectName;
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.
   * ServletContextEvent)
   */
  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // This tries to manually deregister the JDBC driver, which prevents Tomcat 7 from complaining
    // about memory leaks to this class
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    while (drivers.hasMoreElements()) {
      Driver driver = drivers.nextElement();
      try {
        DriverManager.deregisterDriver(driver);
        Utils.getLogger().info("Deregistered driver " + driver);
      } catch (SQLException e) {
        Utils.getLogger().fatal("Error deregistering driver:" + driver, e);
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.servlet.ServletContextListener#contextInitialized(javax.servlet
   * .ServletContextEvent)
   */
  @Override
  public void contextInitialized(ServletContextEvent sce) {
    // Instance the PostgreSQL driver
    try {
      Class.forName("org.postgresql.Driver").newInstance();
    } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e1) {
      Utils.getLogger().error("Unable to instantiate posgresSQL driver.", e1);
    }
    context = sce.getServletContext();

    // read out the project name from the context variable
    projectName = context.getInitParameter(INIT_PARAM_PROJECTNAME);
    Utils.getLogger().info("Context set to: " + projectName);

    // if no context variable was set, fallback to the environment
    if (projectName == null) {
      // Get a handle to the JNDI environment naming context
      Context env;
      try {
        env = (Context) new InitialContext().lookup(CONTEXT_LOOKUP);
        // Get a single value
        projectName = (String) env.lookup(INIT_PARAM_PROJECTNAME);
      } catch (NamingException e) {
        Utils.getLogger().error("Unable to initialize context.", e);
      }
    }

    Utils.getLogger().info("Projectname: " + projectName);
    //        String logDir = FileFinderUtil.getLogDir(projectName);
    //
    //        if (logDir != null) {
    //            Utils.updateLog4jLogDir(logDir);
    //            Utils.getLogger().info("Set LogDir to " + logDir);
    //        }

  }
}
