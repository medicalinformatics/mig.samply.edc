/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.converter;

import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.utils.DateAndTimePatterns;
import de.samply.edc.utils.Utils;
import de.samply.store.AbstractResource;
import de.samply.store.JSONResource;
import de.samply.store.NumberLiteral;
import de.samply.store.Value;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * This is a class to convert data (mostly for data tables) to be saved into the backend (as json).
 */
public class DataConverter {

  public static final String PROPERTY_DATATABLE = "osse.this.is.datatable";
  public static final String PROPERTY_NONREPEATABLERECORD = "osse.this.is.nonrepeatablerecord";
  public static final String PROPERTY_ARRAY = "osse.this.is.array";
  public static final String PROPERTY_ARRAY_ITEM = "arrayEntry";
  public static final String COMBINED_KEY_SEPARATOR = "/";
  public static final String PROPERTY_TYPE = "type";
  public static final String ENUM_DATE_FORMAT = "enumDateFormat";
  public static final String ENUM_TIME_FORMAT = "enumTimeFormat";
  public static final String PROPERTY_COLUMN = "column";

  /**
   * Converts a JSONResource to a datatablerows record representation.
   *
   * @param myJson the my json
   * @param mdrEntityHasValidation the validation data
   * @return the hash map
   */
  public static HashMap<String, Object> convertJsonToRecord(
      JSONResource myJson, JSONResource mdrEntityHasValidation) {

    DatatableRows datatableRows = convertJsonToDatatableRows(myJson, mdrEntityHasValidation);

    if (datatableRows.getRows().isEmpty()) {
      return null;
    }

    return datatableRows.getRows().get(0).getColumns();
  }

  /**
   * Converts a record into a JSONResource for saving the data.
   *
   * @param membersAndValues hashmap of the record
   * @return the JSON resource
   */
  public static JSONResource convertRecordToJsonResource(HashMap<String, Object> membersAndValues) {
    // We store such a nonrepeatable record like a repeatable one with only one row, so we make use
    // of the method below
    DatatableRows datatableRows = new DatatableRows();
    ArrayList<DatatableRow> rows = new ArrayList<>();
    DatatableRow row = new DatatableRow();
    row.setColumns(membersAndValues);
    rows.add(row);
    datatableRows.setRows(rows);

    JSONResource datatableJson = convertDatatableRowsToJsonResource(datatableRows);
    // remove the datatable marker
    datatableJson.removeProperties(PROPERTY_DATATABLE);

    // Add a marker to the resource, so we know what to convert it back to
    datatableJson.addProperty(PROPERTY_NONREPEATABLERECORD, true);

    return datatableJson;
  }

  /**
   * Puts a key-value into a hashmap with correct conversion and casting Used in loading of an
   * entity.
   *
   * @param properties the HashMap
   * @param mdrKey the MDRkey
   * @param value the value
   * @param mdrEntityHasValidation the validation data
   * @return the HashMap with the saved key-value
   */
  public static HashMap<String, Object> putKeyValueInHashMap(
      HashMap<String, Object> properties,
      String mdrKey,
      Value value,
      JSONResource mdrEntityHasValidation) {
    mdrKey = Utils.fixMdrkeyForLoad(mdrKey);

    Date lastChangedDate = convertSecuredTimeStringToDate(value.getValue());
    // TODO: enable this after checking the deeper consequences
    //        else if(value instanceof ResourceIdentifierLiteral) {
    //            properties.put(str, (ResourceIdentifierLiteral) value);
    //        }
    if (lastChangedDate != null) {
      properties.put(mdrKey, lastChangedDate);
    } else if (value instanceof NumberLiteral) {
      properties.put(mdrKey, ((NumberLiteral) value).get());
    } else if (value.asBoolean() != null) {
      properties.put(mdrKey, value.asBoolean());
    } else if (value.asJSONResource() != null) {
      // A JSONResource can be an array, a datatablerows, or a JSONResource
      JSONResource moo = value.asJSONResource();

      if (moo.getProperty(PROPERTY_ARRAY) != null) {
        ArrayList<String> temp = new ArrayList<>();
        List<Value> arrayEntries = moo.getProperties(PROPERTY_ARRAY_ITEM);
        for (Value me : arrayEntries) {
          temp.add(me.getValue());
        }
        properties.put(mdrKey, temp.toArray(new String[temp.size()]));
      } else if (moo.getProperty(PROPERTY_DATATABLE) != null) {
        properties.put(mdrKey, convertJsonToDatatableRows(moo, mdrEntityHasValidation));
      } else if (moo.getProperty(PROPERTY_NONREPEATABLERECORD) != null) {
        HashMap<String, Object> temp = convertJsonToRecord(moo, mdrEntityHasValidation);
        for (Map.Entry<String, Object> recordMemberEntry : temp.entrySet()) {
          String combinedKey =
              mdrKey + COMBINED_KEY_SEPARATOR + Utils.fixMdrkeyForLoad(recordMemberEntry.getKey());
          properties.put(combinedKey, recordMemberEntry.getValue());
        }
      } else {
        properties.put(mdrKey, moo);
      }
    } else {
      properties.put(mdrKey, value.getValue());
    }

    return properties;
  }

  /**
   * provides the mdr validation type and (if available) the date,time,datetime in iso format.
   *
   * @param mdrKey the mdr Key (purified or not)
   * @param value the value to check
   * @param mdrEntityHasValidation the mdrValidationMatrix
   * @return the hash map
   */
  @SuppressWarnings("unused")
  private static HashMap<String, String> provideTypeAndIsoDateValue(
      String mdrKey, String value, JSONResource mdrEntityHasValidation) {
    if (Utils.isNullOrEmpty(value)) {
      return null;
    }

    if (mdrEntityHasValidation == null) {
      return null;
    }

    // if it's "encoded" we decode it to a standard mdr key
    mdrKey = Utils.fixMdrkeyForSave(mdrKey);

    Value validationEntry = mdrEntityHasValidation.getProperty(mdrKey);

    if (validationEntry == null || !(validationEntry instanceof JSONResource)) {
      return null;
    }

    Value validationType = validationEntry.asJSONResource().getProperty(PROPERTY_TYPE);

    if (validationType == null) {
      return null;
    }

    HashMap<String, String> map = new HashMap<>();
    map.put("mdr-type", validationType.getValue());

    Value dateFormat = validationEntry.asJSONResource().getProperty(ENUM_DATE_FORMAT);
    Value timeFormat = validationEntry.asJSONResource().getProperty(ENUM_TIME_FORMAT);

    String pattern;

    if (dateFormat == null) {
      if (timeFormat == null) {
        return map;
      } else {
        pattern = DateAndTimePatterns.getTimePattern(timeFormat.getValue());
      }
    } else {
      if (timeFormat == null) {
        pattern = DateAndTimePatterns.getDatePattern(dateFormat.getValue());
      } else {
        pattern =
            DateAndTimePatterns.getDateTimePattern(dateFormat.getValue(), timeFormat.getValue());
      }
    }

    if (pattern == null) {
      return map;
    }

    SimpleDateFormat dt = new SimpleDateFormat(pattern);
    try {
      Date date = dt.parse(value);
      SimpleDateFormat isoFormat =
          new SimpleDateFormat(DateAndTimePatterns.PATTERN_DATETIME_ISO_8601_WITH_DAYS_AND_SECONDS);
      map.put(DateAndTimePatterns.TIMEZONE_UTC, isoFormat.format(date));
    } catch (ParseException e) {
      Utils.getLogger().warn("Unable to parse:" + value, e);
    }

    return map;
  }

  /**
   * Puts a key-value into a resource with correct casting or conversion of the value Used in saving
   * an entity.
   *
   * @param resource The resource the values shall be saved in
   * @param mdrKey The MDRkey
   * @param value The object value
   * @return The resource with the saved key-value
   */
  public static AbstractResource putKeyValueInResource(
      AbstractResource resource, String mdrKey, Object value) {
    if (value == null) {
      return resource;
    }

    // convert _-key to :-key
    mdrKey = Utils.fixMdrkeyForSave(mdrKey);

    if (value instanceof Boolean) {
      resource.setProperty(mdrKey, (Boolean) value);
    } else if (value instanceof Number) {
      resource.setProperty(mdrKey, (Number) value);
    } else if (value instanceof Date) {
      resource.setProperty(mdrKey, convertDateToSecuredTimeString((Date) value));
    } else if (value instanceof Value) {
      resource.setProperty(mdrKey, (Value) value);
    } else if (value instanceof List) {
      resource.removeProperties(mdrKey);

      for (Object listValue : (Collection<?>) value) {
        if (listValue instanceof Boolean) {
          resource.addProperty(mdrKey, (Boolean) listValue);
        } else if (listValue instanceof Number) {
          resource.addProperty(mdrKey, (Number) listValue);
        } else if (value instanceof Date) {
          resource.addProperty(mdrKey, convertDateToSecuredTimeString((Date) listValue));
        } else if (listValue instanceof Value) {
          resource.addProperty(mdrKey, (Value) listValue);
        } else if (value instanceof DatatableRows) {
          JSONResource myValue = convertDatatableRowsToJsonResource((DatatableRows) listValue);
          resource.addProperty(mdrKey, myValue);
        } else {
          resource.addProperty(mdrKey, listValue.toString());
        }
      }
    } else if (value.getClass().isArray()) {
      JSONResource arrayJson = new JSONResource();
      arrayJson.setProperty(PROPERTY_ARRAY, true);
      // modified to handle problems when value is of Object[]
      try {
        for (String arrayValue : (String[]) value) {
          arrayJson.addProperty(PROPERTY_ARRAY_ITEM, arrayValue);
        }
      } catch (ClassCastException e) {
        for (String arrayValue :
            Arrays.copyOf((Object[]) value, ((Object[]) value).length, String[].class)) {
          arrayJson.addProperty("arrayEntry", arrayValue);
        }
      }

      resource.setProperty(mdrKey, arrayJson);
    } else if (value instanceof DatatableRows) {
      JSONResource myValue = convertDatatableRowsToJsonResource((DatatableRows) value);
      resource.setProperty(mdrKey, myValue);
    } else {
      resource.setProperty(mdrKey, value.toString());
    }

    return resource;
  }

  /**
   * Converts our precious Datatables to a JSONResource.
   *
   * @param datatableRows the data table rows to converted
   * @return the converted data
   */
  public static JSONResource convertDatatableRowsToJsonResource(DatatableRows datatableRows) {
    JSONResource datatableJson = new JSONResource();

    // Add a marker to the resource, so we know what to convert it back to
    datatableJson.addProperty(PROPERTY_DATATABLE, true);

    // Run through the Rows
    for (DatatableRow row : datatableRows.getRows()) {
      JSONResource colJson = new JSONResource();

      // Run through the columns of a row
      for (String colKey : row.getColumns().keySet()) {
        Object colValue = row.getColumns().get(colKey);

        colJson = (JSONResource) putKeyValueInResource(colJson, colKey, colValue);
      }

      // Store the column into the datatableJSON (key = column)
      datatableJson.addProperty(PROPERTY_COLUMN, colJson);
    }

    return datatableJson;
  }

  /**
   * Converts a JSONResource back into a DatatableRows object.
   *
   * @param myJson JSON representation of the data to convert.
   * @param mdrEntityHasValidation the validation data.
   * @return DatatableRows The converted data.
   */
  public static DatatableRows convertJsonToDatatableRows(
      JSONResource myJson, JSONResource mdrEntityHasValidation) {
    DatatableRows datatableRows = new DatatableRows();
    ArrayList<DatatableRow> rows = new ArrayList<>();

    // run through the columns
    for (Value column : myJson.getProperties(PROPERTY_COLUMN)) {
      HashMap<String, Object> columns = new HashMap<>();
      JSONResource colJson = (JSONResource) column;

      // run through the entries of a column
      for (String colKey : colJson.getDefinedProperties()) {
        for (Value colValue : colJson.getProperties(colKey)) {
          columns = putKeyValueInHashMap(columns, colKey, colValue, mdrEntityHasValidation);
        }
      }

      DatatableRow row = new DatatableRow();
      row.setColumns(columns);
      rows.add(row);
    }

    datatableRows.setRows(rows);

    return datatableRows;
  }

  /**
   * Convert to a german date string, using time zone "Europe/Berlin". Using a fixed time zone is
   * important to display the date correctly worldwide
   *
   * @param date the date
   * @return the converted date string, formatted as "dd.MM.yyyy"
   */
  public static String convertToGermanDateString(Date date) {
    return convertToDateString(
        date,
        DateAndTimePatterns.PATTERN_DATE_DIN_5008_WITH_DAYS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert from german date string, assuming time zone "Europe/Berlin".
   *
   * @param value a date string in format "dd.MM.yyyy"
   * @return the converted date, or the given date string if conversion failed
   */
  public static Date convertFromGermanDateString(String value) {
    return convertFromDateString(
        value,
        DateAndTimePatterns.PATTERN_DATE_DIN_5008_WITH_DAYS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert to german date time string, using time zone "Europe/Berlin". Using a fixed time zone is
   * important to display the date correctly worldwide
   *
   * @param date the date
   * @return the converted date time string, formatted as "dd.MM.yyyy HH:mm"
   */
  public static String convertToGermanDateTimeString(Date date) {
    return convertToDateString(
        date,
        DateAndTimePatterns.PATTERN_DATETIME_DIN_5008_WITH_DAYS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert from german time string, assuming time zone "Europe/Berlin".
   *
   * @param value a time string in format "dd.MM.yyyy HH:mm"
   * @return the converted date, or the given time string if conversion failed
   */
  public static Date convertFromGermanDateTimeString(String value) {
    return convertFromDateString(
        value,
        DateAndTimePatterns.PATTERN_DATETIME_DIN_5008_WITH_DAYS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert to date string, using time zone "Europe/Berlin". Using a fixed time zone is important
   * to display the date correctly worldwide
   *
   * @param date the date
   * @return the converted date string, formatted as "yyyy-MM-dd"
   */
  public static String convertToIsoDateString(Date date) {
    return convertToDateString(
        date,
        DateAndTimePatterns.PATTERN_DATE_ISO_8601_WITH_DAYS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert to date time string, using time zone "Europe/Berlin". Using a fixed time zone is
   * important to display the date correctly worldwide
   *
   * @param date the date
   * @return the converted date time string, formatted as "yyyy-MM-dd HH:mm:ss"
   */
  public static String convertToIsoDateTimeString(Date date) {
    return convertToDateString(
        date,
        DateAndTimePatterns.PATTERN_DATETIME_ISO_8601_WITH_DAYS_AND_SECONDS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert from date time string, assuming time zone "Europe/Berlin".
   *
   * @param value a date time string in format "yyyy-MM-dd HH:mm:ss"
   * @return the converted date, or the given date time string if conversion failed
   */
  public static Date convertFromIsoDateTimeString(String value) {
    return convertFromDateString(
        value,
        DateAndTimePatterns.PATTERN_DATETIME_ISO_8601_WITH_DAYS_AND_SECONDS,
        DateAndTimePatterns.TIMEZONE_BERLIN);
  }

  /**
   * Convert a date to a date time string using Coordinated Universal Time (UTC) as time zone. Using
   * a fixed time zone is important to display this data independant from the user's time zone
   *
   * @param date the date to convert
   * @return the converted date time string, formatted as "yyyy-MM-dd'T'HH:mm:ss'Z'"
   */
  public static String convertDateToSecuredTimeString(Date date) {
    return convertToDateString(
        date, DateAndTimePatterns.PATTERN_DATETIME_ISO_UTC, DateAndTimePatterns.TIMEZONE_UTC);
  }

  /**
   * Convert a date time string to a date using Coordinated Universal Time (UTC) as time zone. Using
   * a fixed time zone is important to display this data independant from the user's time zone
   *
   * @param value a date time string in format "yyyy-MM-dd'T'HH:mm:ss'Z'"
   * @return the date
   */
  public static Date convertSecuredTimeStringToDate(String value) {
    //Using regex checks time format "yyyy-MM-dd'T'HH:mm:ss'Z'" like static PATTERN_DATETIME_ISO_UTC
    if (value != null && value.matches("(\\d{4}-\\d{2}-\\d{2})T(\\d{2}:\\d{2}:\\d{2})Z")) {
      return convertFromDateString(
          value, DateAndTimePatterns.PATTERN_DATETIME_ISO_UTC, DateAndTimePatterns.TIMEZONE_UTC);
    } else {
      return null;
    }
  }

  /**
   * Convert from date string to date.
   *
   * @param value a date string in format "yyyy-MM-dd"
   * @param pattern the pattern of the date string
   * @param timeZone the timezone of the date string
   * @return the converted date, or the given date string if conversion failed
   */
  public static Date convertFromDateString(String value, String pattern, String timeZone) {
    SimpleDateFormat format;
    format = new SimpleDateFormat(pattern);
    format.setTimeZone(TimeZone.getTimeZone(timeZone));

    try {
      return format.parse(value);

    } catch (ParseException e) {
      Utils.getLogger().debug("Unable to parse:" + value, e);
      return null;
    }
  }

  /**
   * Converts date to date string.
   *
   * @param date the date
   * @param pattern the pattern to transform to
   * @param timeZone the timezone used
   * @return the string
   */
  public static String convertToDateString(Date date, String pattern, String timeZone) {
    SimpleDateFormat format;
    format = new SimpleDateFormat(pattern);
    format.setTimeZone(TimeZone.getTimeZone(timeZone));
    return format.format(date);
  }
}
