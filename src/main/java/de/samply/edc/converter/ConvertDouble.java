/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.converter;

import de.samply.edc.utils.Utils;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/** Converter to take care of German-English mixture for number typing (aka the , and . problem) */
public class ConvertDouble implements Converter {

  public static final String NO_VALUE_ERROR = "convert_double_no_value_error";
  public static final String VALIDATION_ERROR_HEADER = "validation_error_header";

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {

    if (Utils.isNullOrEmpty(value)) {
      return null;
    }

    try {
      return Double.parseDouble(value);
    } catch (Exception e) {
      Utils.getLogger().debug("Problem parsing value as double. Replacing , and . in:" + value);
      value = value.replace(",", ".");
    }

    try {
      return Double.parseDouble(value);
    } catch (Exception e) {
      Utils.getLogger().debug("Unable to parse:" + value);
      String wrongRange = Utils.getResourceBundleString(NO_VALUE_ERROR);
      String summary = Utils.getResourceBundleString(VALIDATION_ERROR_HEADER);

      FacesMessage msg = new FacesMessage(summary, wrongRange);
      msg.setSeverity(FacesMessage.SEVERITY_ERROR);
      throw new ConverterException(msg);
    }
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value == null) {
      return null;
    }

    return value.toString();
  }
}
