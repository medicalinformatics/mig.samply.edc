/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.control;

import de.samply.edc.utils.Utils;
import de.samply.store.DatabaseConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.faces.context.FacesContext;
import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.core.config.Configurator;

/** Abstract class for applicationBeans. */
public abstract class AbstractApplicationBean implements Serializable {

  public static final String CONFIG_PARAM_INSTANCE_PROJECT = "instance.project";
  public static final String CONFIG_PARAM_PROJECT_NAME = "project.name";
  public static final String CONFIG_PARAM_FALLBACK_ROOT = "project.fallBackRootPath";
  public static final String PATH_WEB_INF = "/WEB-INF";
  /** The apache configuration. */
  protected transient Configuration config;

  /** The supported locales. */
  protected List<String> supportedLocales = null;

  /** Initialization of the configuration and setting the supported locales. */
  public void init() {
    // This line defines that PostgreSQL 9.3 is used.
    // TODO: It will be set to this by default in a later version of the
    // backend, then it can be deleted here
    DatabaseConstants.PSQLVersionCode = 1;

    config = Utils.getConfig();

    if (config.getProperty(CONFIG_PARAM_INSTANCE_PROJECT) != null) {
      config.addProperty(
          CONFIG_PARAM_PROJECT_NAME, config.getProperty(CONFIG_PARAM_INSTANCE_PROJECT));
    }
    config.addProperty(CONFIG_PARAM_FALLBACK_ROOT, Utils.getRealPath(PATH_WEB_INF));

    // Log4J2 (Backend-debug reports. Set to debug in log4j2.xml if you need
    // them)
    Configurator.initialize("moo", "file://" + Utils.findConfigurationFile("log4j2.xml"));

    Iterator<Locale> it = FacesContext.getCurrentInstance().getApplication().getSupportedLocales();
    supportedLocales = new ArrayList<>();

    while (it.hasNext()) {
      Locale locale = it.next();
      supportedLocales.add(locale.getLanguage());
    }

    if (supportedLocales.isEmpty()) {
      supportedLocales.add(
          FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage());
    }
  }

  /**
   * Gets the list of supported locales.
   *
   * @return the supported locales
   */
  public List<String> getSupportedLocales() {
    return supportedLocales;
  }

  /**
   * Checks if a given locale is supported.
   *
   * @param locale the locale to check
   * @return true if it is supported, false otherwise
   */
  public Boolean isASupportedLocale(Locale locale) {
    return supportedLocales.contains(locale.getLanguage());
  }

  /**
   * Gets the config.
   *
   * @return the config
   */
  public Configuration getConfig() {
    return config;
  }

  /**
   * Method to return a page title. This MUST be overwritten in your own class
   *
   * @param currentFormName the current form name
   * @param locale the locale
   * @return the page title name
   */
  public abstract String getPageTitleName(String currentFormName, Locale locale);
}
