/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.control;

import de.samply.edc.model.Entity;
import de.samply.edc.utils.Utils;
import de.samply.store.BasicDB;
import de.samply.store.DatabaseConstants;
import de.samply.store.DatabaseModel;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Abstract for Database class This is used as a wrapper for the de.samply.store model to call its
 * methods due toe serializable reasons
 *
 * @param <T> the de.samply.store.model to be used
 */
public abstract class AbstractDatabase<T extends DatabaseModel<?>> implements Serializable {

  public static final String FILENAME_BACKEND_XML = "backend.xml";
  public static final String USERNAME_ADMIN = "admin";
  public static final String ROLENAME_SYSTEM = "SYSTEM";
  public static final String CURRENT_OBJECT_USER = "user";
  public static final String CURRENT_OBJECT_ROLE = "role";
  /** The de.samply.store model */
  private transient T model = null;

  /** sessionBean field. */
  private AbstractSessionBean sessionBean;

  /** Boolean to define if something is called by the system itself. */
  private Boolean isSystemAction = false;

  /**
   * Instantiates a new abstract database.
   *
   * @param sessionBeanBase the session bean base
   */
  public AbstractDatabase(AbstractSessionBean sessionBeanBase) {
    sessionBean = sessionBeanBase;
  }

  /**
   * Instantiates a new abstract database.
   *
   * @param isSystemAction if the system itself accesses data we use a special system role.
   */
  public AbstractDatabase(Boolean isSystemAction) {
    this.isSystemAction = true;
  }

  /**
   * Gets the store model.
   *
   * @param file the configuration file (backend.xml)
   * @return the de.samply.store model
   */
  protected abstract T get(String file);

  /**
   * Gets the database model and makes sure that it's not null.
   *
   * @return the de.samply.store model
   */
  public T getDatabaseModel() {
    if (model == null) {
      try {
        // Set Backend to use pre JSONB model
        DatabaseConstants.PSQLVersionCode = 1;

        String configFile = Utils.findConfigurationFile(FILENAME_BACKEND_XML);
        model = get(configFile);

        if (isSystemAction) {
          model.injectLogin(USERNAME_ADMIN);

          ResourceQuery query = new ResourceQuery(BasicDB.Type.Role);
          query.add(Criteria.Equal(BasicDB.Type.Role, BasicDB.Role.RoleType, ROLENAME_SYSTEM));
          Resource theRole = this.getResources(query).get(0);
          model.selectRole(theRole.getId());
        } else {
          if (sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
            model.injectLogin(
                (String)
                    ((Entity) sessionBean.getCurrentObject(CURRENT_OBJECT_USER))
                        .getProperty(BasicDB.User.Username));
            if (Utils.getSB().getCurrentObject(CURRENT_OBJECT_ROLE) != null) {
              int roleId = ((Entity) Utils.getSB().getCurrentObject(CURRENT_OBJECT_ROLE)).getId();
              model.selectRole(roleId);
            }
          }
        }
      } catch (DatabaseException e) {
        Utils.getLogger().error("Unable to setup new database model, returning null", e);
      }
    }

    return model;
  }

  /** Executes a rollback. */
  public void rollback() {
    try {
      getDatabaseModel().rollback();
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper rollback!", e);
    }
  }

  /** Begin transaction. */
  public void beginTransaction() {
    try {
      getDatabaseModel().beginTransaction();
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper beginTransaction!", e);
    }
  }

  /**
   * Saves one or more resources. This is a shortcut to the three methods: beginTranscation,
   * saveOrUpdateResource, commit.
   *
   * @param resources the resources to save
   */
  public void save(Resource... resources) {
    if (resources.length < 1) {
      return;
    }

    beginTransaction();
    saveOrUpdateResource(resources);
    commit();
  }

  /**
   * Deletes one or more resources.
   *
   * @param resources the resources to delete
   */
  public void deleteResource(Resource... resources) {
    try {
      getDatabaseModel().deleteResources(resources);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper deleteResource!", e);
      for (Resource resource : resources) {
        Utils.getLogger()
            .debug("Resource in use during exception: " + resource.getIdentifier().getValue());
      }
    }
  }

  /**
   * Saves or updates one or more resources.
   *
   * @param resources the resources to save or update
   */
  public void saveOrUpdateResource(Resource... resources) {
    if (sessionBean != null && sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
      // refreshTransactionkey(sessionBean.getCurrentUser().getUserID());
    }
    try {
      for (Resource resource : resources) {
        getDatabaseModel().saveOrUpdateResource(resource);
      }
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper saveOrUpdateResource()", e);
      for (Resource resource : resources) {
        Utils.getLogger()
            .debug("Resource in use during exception: " + resource.getIdentifier().getValue());
      }
    }
  }

  /**
   * Gets a resource by identifier.
   *
   * @param identifier the identifier
   * @return the resource
   */
  public Resource getResourceByIdentifier(String identifier) {
    try {
      return getDatabaseModel().getResourceByIdentifier(identifier);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper getResourceByIdentifier()", e);
      Utils.getLogger().debug("Resource identifier: " + identifier);
    }
    return null;
  }

  /**
   * Creates a resource of a given type.
   *
   * @param type the type of the resource to be created
   * @return the newly created resource
   */
  public Resource createResource(String type) {
    return getDatabaseModel().createResource(type);
  }

  /**
   * Gets a resource of a given type and id.
   *
   * @param type the type
   * @param id the id
   * @return the resource
   */
  public Resource getResource(String type, int id) {
    if (sessionBean != null && sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
      // refreshTransactionkey(sessionBean.getCurrentUser().getUserID());
    }
    try {
      return getDatabaseModel().getResource(type, id);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper getResource type = " + type + " id=" + id + "!", e);
    }
    return null;
  }

  /**
   * Gets all resources of a given type.
   *
   * @param type the type
   * @return the list of resources
   */
  public ArrayList<Resource> getResources(String type) {
    if (sessionBean != null && sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
      // refreshTransactionkey(sessionBean.getCurrentUser().getUserID());
    }
    try {
      return getDatabaseModel().getResources(type);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper getResources type=" + type + "!", e);
    }
    return new ArrayList<>();
  }

  /**
   * Gets all resources by a given criteria.
   *
   * @param criteria the criteria
   * @return the list of resources
   */
  public ArrayList<Resource> getResources(ResourceQuery criteria) {
    if (sessionBean != null && sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
      // refreshTransactionkey(sessionBean.getCurrentUser().getUserID());
    }

    try {
      return getDatabaseModel().getResources(criteria);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper getResources criteria=" + criteria + "!", e);
    }
    return new ArrayList<>();
  }

  /**
   * Gets all resources by a given criteria.
   *
   * @param criteria the criteria
   * @return the amount of resources
   */
  public Integer countResources(ResourceQuery criteria) {
    if (sessionBean != null && sessionBean.getCurrentObject(CURRENT_OBJECT_USER) != null) {
      // refreshTransactionkey(sessionBean.getCurrentUser().getUserID());
    }

    try {
      return getDatabaseModel().countResources(criteria);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper countResources criteria=" + criteria + "!", e);
    }
    return 0;
  }

  /** Performs a logout in the store.model */
  public void logout() {
    getDatabaseModel().logout();
  }

  /** Performs a commit. */
  public void commit() {
    try {
      getDatabaseModel().commit();
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper commit!", e);
    }
  }

  /**
   * Change user password.
   *
   * @param userResource the user resource
   * @param currentPassword the current password
   * @param newPassword the new password
   * @return the updated user resource
   */
  public Resource changeUserPassword(
      Resource userResource, String currentPassword, String newPassword) {
    try {
      return getDatabaseModel().changeUserPassword(userResource, currentPassword, newPassword);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper changePassword!", e);
    }

    return null;
  }

  /**
   * Force change user password by admin without checking for a current password.
   *
   * @param userResource the user resource
   * @param newPassword the new password
   * @return the updated user resource
   */
  public Resource changeUserPasswordByAdmin(Resource userResource, String newPassword) {
    try {
      return getDatabaseModel().changeUserPasswordByAdmin(userResource, newPassword);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper changePassword!", e);
    }

    return null;
  }

  /**
   * Performs a login in the store.model.
   *
   * @param username the username
   * @param password the password
   * @throws DatabaseException the database exception
   */
  public void login(String username, String password) throws DatabaseException {
    getDatabaseModel().login(username, password);
  }

  /**
   * Creates a new User with the given password. You need to save the returned resource in order to
   * store the new user in the database;
   *
   * @param username the username
   * @param password the password
   * @return the created user resource
   */
  public Resource createUser(String username, String password) {
    try {
      return getDatabaseModel().createUser(username, password);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error(
              "EXCEPTION in Database wrapper createUser user=" + username, e);
    }

    return null;
  }

  /**
   * Generates a new activation code and returns the *unsaved* modified user with the given
   * username.
   *
   * @param email the email
   * @return the resource
   */
  public Resource forgotPassword(String email) {
    try {
      return getDatabaseModel().forgotPassword(email);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper forgotPassword email = " + email, e);
    }
    return null;
  }

  /**
   * Activates the user with the given activationCode (which is unique) and initializes the user
   * with the given password (and activates the user).
   *
   * @param password the password
   * @param activationCode the activation code
   * @return the user resource
   */
  public Resource activateUser(String password, String activationCode) {
    try {
      return getDatabaseModel().activateUser(password, activationCode);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper activateUser code = " + activationCode, e);
    }
    return null;
  }

  /**
   * Creates a new user with the given username, password and salt. You need to save the returned
   * resource in order to store the new user in the database;
   *
   * @param user the user
   * @param password the password
   * @param salt the salt
   * @return the resource
   */
  public Resource importUser(String user, String password, String salt) {
    try {
      return getDatabaseModel().createUser(user, password, salt);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper import/createUser user=" + user,e);
    }

    return null;
  }

  /**
   * Find a user by his username.
   *
   * @param username the user name
   * @return the user resource
   */
  public Resource findUser(String username) {
    ResourceQuery criteria = new ResourceQuery(BasicDB.Type.User);
    criteria.add(Criteria.Equal(BasicDB.Type.User, BasicDB.User.Username, username));
    ArrayList<Resource> theUsers = getResources(criteria);
    if (theUsers.isEmpty()) {
      return null;
    }

    return theUsers.get(0);
  }

  /**
   * checks is a username exists.
   *
   * @param username the user name
   * @return true, if successful
   */
  public boolean userExists(String username) {
    return findUser(username) != null;
  }

  /**
   * Reloads the given resource. Leaves the given resource instance as it is.
   *
   * @param resource the resource
   * @return the resource
   */
  public Resource reloadResource(Resource resource) {
    try {
      return getDatabaseModel().reloadResource(resource);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error("EXCEPTION in Database wrapper reloadResource resource=" + resource + "!", e);
    }
    return null;
  }

  /**
   * Saves the configuration objects as "name".
   *
   * @param name the name
   * @param config the config
   */
  public void saveConfig(String name, JSONResource config) {
    try {
      getDatabaseModel().saveConfig(name, config);
    } catch (DatabaseException e) {
      Utils.getLogger()
          .error(
              "EXCEPTION in Database wrapper saveConfig criteria=" + name + " , " + config + "!",
              e);
    }
  }

  /**
   * Returns the configuration object saved under the name "name".
   *
   * @param name the name
   * @return the config
   */
  public JSONResource getConfig(String name) {
    try {
      return getDatabaseModel().getConfig(name);
    } catch (DatabaseException e) {
      Utils.getLogger().error("EXCEPTION in Database wrapper getConfig criteria=" + name + "!", e);
    }
    return new JSONResource();
  }

  /**
   * Executes the specified action with the specified parameters, yet used to change the status of a
   * form. For example to name="sign" a given form from status 1 to 2
   *
   * @param formResource the form resource
   * @param statusResource the goal status resource
   * @throws DatabaseException the database exception
   */
  public void executeFormAction(Resource formResource, Resource statusResource)
      throws DatabaseException {
    getDatabaseModel().executeAction("changeStatus", formResource, statusResource);
  }

  /**
   * Adds a log entry to the logbook. TODO: Yet to be implemented on backend side.
   *
   * @param userUri The URI of the user
   * @param action The action performed
   */
  public void addLog(String userUri, String action) {}

  /** Starts the caching in the database model. */
  public void startCaching() {
    getDatabaseModel().startCaching();
  }

  /** Stops the caching in the database model. */
  public void stopCaching() {
    getDatabaseModel().stopCaching();
  }
}
