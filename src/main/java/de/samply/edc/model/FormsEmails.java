/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;
import de.samply.edc.control.Mailer;
import de.samply.edc.utils.Utils;
import java.io.File;
import java.io.Serializable;

/** Class for certain emails that are constructed by templates. */
public class FormsEmails implements Serializable {

  public static final String NAMESPACE_EMAILS = "samply.emails";
  public static final String CONFIG_PARAM_ACTIVATION_TEXT = "mail.text.activation";
  public static final String NEWUSER = ".newuser";
  public static final String COFIG_PARAM_ACTIVATION_SUBJECT = "mail.subject.activation";
  public static final String CONFIG_PARAM_FORGOTPW_TEXT = "mail.text.forgotpassword";
  public static final String FORGOTPASSWORD = ".forgotpassword";
  public static final String CONFIG_PARAM_FORGOTPW_SUBJECT = "mail.subject.forgotpassword";

  /**
   * Send new user email. This sends an email to the new user containing his activation code.
   *
   * @param email the email of the new user
   * @param name the name of the new user
   * @param activationcode the activation code
   */
  public void sendNewUser(String email, String name, String activationcode) {
    String relativeWebPath;

    if (Utils.getLanguage().equals("de")) {
      relativeWebPath = "/mailtemplate/emails_de.soy";
    } else {
      relativeWebPath = "/mailtemplate/emails_de.soy";
    }

    String absoluteDiskPath = Utils.getRealPath(relativeWebPath);
    SoyFileSet sfs = SoyFileSet.builder().add(new File(absoluteDiskPath)).build();
    SoyTofu tofu = sfs.compileToTofu();
    SoyTofu simpleTofu = tofu.forNamespace(NAMESPACE_EMAILS);
    SoyMapData data =
        new SoyMapData("url", Utils.getBasedir() + "activate.xhtml?code=" + activationcode);

    String text =
        Utils.getAB().getConfig().getString(CONFIG_PARAM_ACTIVATION_TEXT)
            + "\n\n"
            + simpleTofu.newRenderer(NEWUSER).setData(data).render();

    Mailer.sendEmail(
        Utils.getAB().getConfig().getString(COFIG_PARAM_ACTIVATION_SUBJECT),
        text,
        new String[] {email});
  }

  /**
   * Send an email to the user who forgot his password with a new activation code.
   *
   * @param email the email of the forgetful companion
   * @param activationcode the activationcode
   */
  public void sendForgotPassword(String email, String activationcode) {
    String relativeWebPath;

    if (Utils.getLanguage().equals("de")) {
      relativeWebPath = "/mailtemplate/emails_de.soy";
    } else {
      relativeWebPath = "/mailtemplate/emails_de.soy";
      // relativeWebPath = "/mailtemplate/emails_en.soy";
    }

    String absoluteDiskPath = Utils.getRealPath(relativeWebPath);
    SoyFileSet sfs = SoyFileSet.builder().add(new File(absoluteDiskPath)).build();
    SoyTofu tofu = sfs.compileToTofu();
    SoyTofu simpleTofu = tofu.forNamespace(NAMESPACE_EMAILS);
    SoyMapData data =
        new SoyMapData("url", Utils.getBasedir() + "activate.xhtml?code=" + activationcode);

    String text =
        Utils.getAB().getConfig().getString(CONFIG_PARAM_FORGOTPW_TEXT)
            + "\n\n"
            + simpleTofu.newRenderer(FORGOTPASSWORD).setData(data).render();

    Mailer.sendEmail(
        Utils.getAB().getConfig().getString(CONFIG_PARAM_FORGOTPW_SUBJECT),
        text,
        new String[] {email});
  }
}
