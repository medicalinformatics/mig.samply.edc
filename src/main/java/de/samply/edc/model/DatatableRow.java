/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/** Model class for a data table row. */
public class DatatableRow implements Serializable {

  /** The columns of a row. */
  private HashMap<String, Object> columns = new HashMap<>();

  /**
   * getColumns used to be called getEntries in the past. As there may be still forms out there
   * which use the old naming, we need to keep this for compatibility reasons.
   *
   * @return the entries
   */
  @Deprecated
  public HashMap<String, Object> getEntries() {
    return columns;
  }

  /**
   * setColumns used to be called setEntries in the past. As there may be still forms out there
   * which use the old naming, we need to keep this for compatibility reasons.
   *
   * @param columns the columns
   */
  @Deprecated
  public void setEntries(HashMap<String, Object> columns) {
    this.columns = columns;
  }

  /**
   * Gets the columns of this row.
   *
   * @return the columns
   */
  public HashMap<String, Object> getColumns() {
    return columns;
  }

  /**
   * Sets the columns of this row.
   *
   * @param columns the columns
   */
  public void setColumns(HashMap<String, Object> columns) {
    this.columns = columns;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder text = new StringBuilder();
    for (Map.Entry<String, Object> entry : columns.entrySet()) {
      text.append("key=")
          .append(entry.getKey())
          .append(" value=")
          .append(entry.getValue())
          .append(", ");
    }

    if ("".equals(text.toString())) {
      text = new StringBuilder("Empty Column, ");
    }

    text = new StringBuilder(text.substring(0, text.length() - 2));
    return text.toString();
  }
}
