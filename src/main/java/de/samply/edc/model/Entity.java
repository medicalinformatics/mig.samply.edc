/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Attributes;
import de.samply.edc.catalog.Vocabulary.Generic;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.converter.DataConverter;
import de.samply.edc.utils.Utils;
import de.samply.store.BasicDB;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.Value;
import de.samply.store.query.Criteria;
import de.samply.store.query.OrCriteria;
import de.samply.store.query.ResourceQuery;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.context.FacesContext;
import org.apache.commons.configuration.Configuration;

/** Model class for an entity. */
public class Entity implements Serializable, Comparable<Entity> {

  /** The final instantiated classes of entities saved by OSSEVocabulary.Type.* type */
  private static HashMap<String, Class<? extends Entity>> classes;
  /** The found records. */
  private HashMap<String, HashMap<String, Object>> foundRecords;
  /** The backend resource of this entity. */
  private Resource resource;
  /** The backedn URI of the resource. */
  private String resourceUri;
  /** The database backend wrapper model. */
  private AbstractDatabase<?> database;
  /** The properties of the entity. */
  private HashMap<String, Object> properties;
  /** A list of properties to be removed in the next save. */
  private List<String> removedProperties;
  /**
   * Parents storage as HashMap ofString -> Entity:
   *
   * <p>- String: The type of the parent as of terms in OSSEVocabulary.Type.*
   *
   * <p>- Entity: The parent entity
   */
  private HashMap<String, Entity> parents;
  /**
   * Children storage as HashMap of String -> HashMap:
   *
   * <p>- String: The type of the children as of terms in OSSEVocabulary.Type.*
   *
   * <p>- HashMap of String -> Entity containing the children entities:
   *
   * <p>-- String: Either the URI of the child or the childrenHashPrimaryKey
   *
   * <p>-- Entity: The child entity
   */
  private HashMap<String, HashMap<String, Entity>> children;
  /**
   * childrenHashPrimaryKey can be used if you want to organize the children HashMap yourself based
   * on a certain attribute as key, which has to be unique among the children. By standard the URI
   * of the child's entity is used as "key"
   *
   * <p>String - The type of the children as of terms in OSSEVocabulary.Type.* String - The key as
   * of terms of an attribute of the Entity, e.g. OSSEVocabulary.Form.Name
   */
  private HashMap<String, String> childrenHashPrimaryKey = null;
  /**
   * The sorted list of children This is used on several pages and tables. Problem is that you
   * cannot calculate this from the hashmap on a getter, because JSF calls this method three times
   * for some unknown reason. So we have to make sure this list gets only calculated once, and the
   * next time the getter is called, the already calculated list gets returned
   *
   * <p>String: The type of the children as of terms in OSSEVocabulary.Type.*
   *
   * <p>List: ArrayList containing the entities
   */
  private HashMap<String, List<Entity>> sortedChildren;
  /**
   * A sublist of children entities in case you want to filter the complete list with something
   *
   * <p>String: The type of the children as of terms in OSSEVocabulary.Type.*
   *
   * <p>List: ArrayList containing the entities
   */
  private HashMap<String, ArrayList<Entity>> subListOfChildren;
  /**
   * This value is filled by the UI on creation of a new entity used to then transform into a local
   * pseudonym.
   */
  private String tmpID;
  /** The backend OSSEVocabulary.Type.* of this entity */
  private String type;
  /** The number of parents this entity has. */
  private int numberParents;

  /**
   * Instantiates a new entity.
   *
   * @param database the database backend wrapper
   * @param type the OSSEVocabulary.Type.* type
   */
  public Entity(AbstractDatabase<?> database, String type) {
    this.database = database;
    this.type = type;
    init();
  }

  /**
   * Instantiates a new entity.
   *
   * @param database the database backend wrapper
   * @param type the OSSEVocabulary.Type.* type
   * @param entityUri the entity URI
   */
  public Entity(AbstractDatabase<?> database, String type, String entityUri) {
    this.database = database;
    this.resource = database.getResourceByIdentifier(entityUri);
    this.resourceUri = entityUri;
    this.type = type;
    init();
  }

  /**
   * Instantiates a new entity.
   *
   * @param database the database backend wrapper
   * @param type the OSSEVocabulary.Type.* type
   * @param entityResource the entity backend resource
   */
  public Entity(AbstractDatabase<?> database, String type, Resource entityResource) {
    this.database = database;
    this.resource = entityResource;
    this.resourceUri = entityResource.getValue();
    this.type = type;
    init();
  }

  /**
   * Instantiates a new entity.
   *
   * @param database the database backend wrapper
   * @param type the OSSEVocabulary.Type.*type
   * @param entityID the entity backend id
   */
  public Entity(AbstractDatabase<?> database, String type, Integer entityID) {
    this.database = database;
    this.resource = database.getResource(type, entityID);
    this.resourceUri = this.resource.getValue();
    this.type = type;
    init();
  }

  /**
   * Checks if an entity exists by property and is the child of a certain parent by backend ID.
   *
   * @param database Database handler
   * @param entityType the OSSEVocabulary.Type.* Type of entity to check
   * @param property the property to check
   * @param value the value of the property to check
   * @param parentType the OSSEVocabulary.Type.* type of the parent
   * @param parentID the backend id of the parent
   * @return the backend resource
   */
  public static Resource entityExistsByPropertyAsChildOfParentID(
      AbstractDatabase<?> database,
      String entityType,
      String property,
      String value,
      String parentType,
      Integer parentID) {
    ResourceQuery query = new ResourceQuery(entityType);
    query.add(Criteria.Equal(entityType, property, value));
    query.add(Criteria.Equal(parentType, BasicDB.ID, parentID));
    ArrayList<Resource> found = database.getResources(query);

    if (found.size() > 0) {
      return found.get(0);
    } else {
      return null;
    }
  }

  /**
   * Checks if any entity exists by property and is the child of a certain parent by backend ID.
   *
   * @param database Database handler
   * @param entityType the OSSEVocabulary.Type.* Type of entity to check
   * @param property the property to check
   * @param listOfPossibleValues the values of the property to check
   * @param parentType the OSSEVocabulary.Type.* type of the parent
   * @param parentID the backend id of the parent
   * @return the backend resource
   */
  public static Resource entityExistsByPropertyAsChildOfParentID(
      AbstractDatabase<?> database,
      String entityType,
      String property,
      List<String> listOfPossibleValues,
      String parentType,
      Integer parentID) {
    ResourceQuery query = new ResourceQuery(entityType);

    query.add(Criteria.Equal(parentType, BasicDB.ID, parentID));

    OrCriteria or = Criteria.Or();

    for (String value : listOfPossibleValues) {
      or.add(Criteria.Equal(entityType, property, value));
    }

    if (!or.getAll().isEmpty()) {
      query.add(or);
    }

    ArrayList<Resource> found = database.getResources(query);

    if (found.size() > 0) {
      return found.get(0);
    } else {
      return null;
    }
  }

  /** Init. */
  private void init() {
    properties = new HashMap<>();
    children = new HashMap<>();
    childrenHashPrimaryKey = new HashMap<>();
    parents = new HashMap<>();

    numberParents = DatabaseDefinitions.getParent(type).size();
  }

  /**
   * The resource can get "lost" for some reason (serialization probably) so we will have to find it
   * again aka reload it.
   */
  public void reloadResource() {
    if (resource == null && resourceUri != null) {
      resource = getDatabase().getResourceByIdentifier(resourceUri);
    }
  }

  @Deprecated
  public Boolean load() {
    return load(database.getConfig("osse"));
  }

  /**
   * Method to load the properties of an entity.
   *
   * @return true on success
   */
  public Boolean load(JSONResource myConfig) {
    JSONResource mdrEntityHasValidation =
        (JSONResource) myConfig.getProperty(Vocabulary.Config.mdrEntityHasValidation);
    //        JSONResource mdrEntityHasValidation = new JSONResource();

    for (String mdrKey : resource.getDefinedProperties()) {
      for (Value value : resource.getProperties(mdrKey)) {
        properties =
            DataConverter.putKeyValueInHashMap(properties, mdrKey, value, mdrEntityHasValidation);
      }
    }

    loadLastChanged(myConfig); // TODO: this should be replaced in the future by
    // versionizing support of the backend

    return true;
  }

  @Deprecated
  public Boolean loadQuick() {
    return loadQuick(database.getConfig("osse"));
  }

  /**
   * Same as load() but without loadLastChanged().
   *
   * @return true on success
   */
  public Boolean loadQuick(JSONResource myConfig) {
    JSONResource mdrEntityHasValidation =
        (JSONResource) myConfig.getProperty(Vocabulary.Config.mdrEntityHasValidation);

    for (String mdrKey : resource.getDefinedProperties()) {
      for (Value value : resource.getProperties(mdrKey)) {
        properties =
            DataConverter.putKeyValueInHashMap(properties, mdrKey, value, mdrEntityHasValidation);
      }
    }

    return true;
  }

  /**
   * Clears the properties of an entity.
   *
   * @return true on success
   */
  public Boolean unload() {
    properties.clear();
    return true;
  }

  /**
   * Saves the entity and its children This must be surrounded by a transaction.
   *
   * @return true on success
   */
  public Boolean saveOrUpdateMeAndChildren(JSONResource myConfig) {
    if (!saveOrUpdate()) {
      return false;
    }

    if (Utils.isNullOrEmpty(getChildrenTypes())) {
      return true;
    }

    for (String childType : getChildrenTypes()) {
      List<Entity> someChildren = getChildren(childType, myConfig);
      for (Entity e : someChildren) {
        e.saveOrUpdate();
      }
    }
    return true;
  }

  /**
   * Saves the entity This must be surrounded by a transaction.
   *
   * @return true on success
   */
  public Boolean saveOrUpdate() {
    if (getResourceUri() == null) {
      return store();
    }

    if (getResource() == null) {
      reloadResource();
      if (getResource() == null) {
        return store();
      }
    }

    return change();
  }

  /**
   * Prepares saving of an entity by creating a new resource in the backend and then calls the
   * internal save method.
   *
   * @return true on success
   */
  private Boolean store() {
    resource = database.createResource(type);
    foundRecords = new HashMap<>();

    for (Map.Entry<String, Object> entry : properties.entrySet()) {
      if (entry.getValue() == null) {
        continue;
      }

      String key = Utils.fixMdrkeyForSave(entry.getKey());

      changeOrSave(key, entry.getValue());
    }

    changeOrSaveRecords();

    database.saveOrUpdateResource(resource);
    resourceUri = resource.getValue();

    return true;
  }

  /**
   * Prepares saving of an existing entity in the backend and then calls the internal save method.
   *
   * @return true on success
   */
  private Boolean change() {
    foundRecords = new HashMap<>();

    if (removedProperties != null) {
      for (String removeKey : removedProperties) {
        resource.removeProperties(Utils.fixMdrkeyForSave(removeKey));
      }

      removedProperties = null;
    }

    for (Map.Entry<String, Object> entry : properties.entrySet()) {
      String key = Utils.fixMdrkeyForSave(entry.getKey());

      if (entry.getValue() == null) {
        resource.removeProperties(key);
        continue;
      }

      changeOrSave(key, entry.getValue());
    }

    changeOrSaveRecords();

    database.saveOrUpdateResource(resource);
    return true;
  }

  /**
   * Checks if a given mdrkey is of a record.
   *
   * @param key the mdrkey
   * @return the boolean
   */
  private Boolean isNonRepeatableRecord(String key) {
    return key != null && key.contains("/") && key.contains("record");
  }

  /** Change or save records. */
  private void changeOrSaveRecords() {
    if (foundRecords.isEmpty()) {
      return;
    }

    for (Map.Entry<String, HashMap<String, Object>> entry : foundRecords.entrySet()) {
      HashMap<String, Object> membersAndValues = entry.getValue();
      JSONResource myValue = DataConverter.convertRecordToJsonResource(membersAndValues);
      resource.setProperty(entry.getKey(), myValue);
    }
  }

  /**
   * private method to save a key and its value of this entity in the backend.
   *
   * @param key the key
   * @param value the value
   */
  private void changeOrSave(String key, Object value) {
    // If a key is actually part of a record, store that into a temporary
    // record-member hash
    // to save after all were found
    if (isNonRepeatableRecord(key)) {
      String[] keys = key.split("/");
      String recordMdrId = keys[0];
      String memberMdrId = keys[1];

      if (foundRecords.containsKey(recordMdrId)) {
        foundRecords.get(recordMdrId).put(memberMdrId, value);
      } else {
        HashMap<String, Object> temp = new HashMap<>();
        temp.put(memberMdrId, value);
        foundRecords.put(recordMdrId, temp);
      }

      return;
    }

    resource = (Resource) DataConverter.putKeyValueInResource(resource, key, value);
  }

  /**
   * Declares an entity deleted.
   *
   * @return true on success.
   */
  public Boolean delete() {
    if (resourceUri == null) {
      return false;
    }

    if (resource == null) {
      reloadResource();
    }

    properties.put(Generic.isDeleted, 1);
    resource.setProperty(Generic.isDeleted, 1);
    database.save(resource);

    return true;
  }

  /**
   * Removes the "deleted" declaration from an entity.
   *
   * @return true on success
   */
  public Boolean undelete() {
    if (resourceUri == null) {
      return false;
    }

    if (resource == null) {
      reloadResource();
    }

    properties.put(Generic.isDeleted, 0);
    resource.setProperty(Generic.isDeleted, 0);
    database.save(resource);

    return true;
  }

  /**
   * Checks if the entity is marked as deleted. Due to compatibility reasons, the deletion marker
   * can be a number (as it is now), but also a string (as it used to be)
   *
   * @return true|false
   */
  public Boolean getIsDeleted() {
    Object test = properties.get(Generic.isDeleted);
    if (test == null) {
      return false;
    }

    Number isDeleted;
    if (test instanceof String) {
      isDeleted = Integer.valueOf((String) test);
    } else if (test instanceof Number) {
      isDeleted = (Number) test;
    } else {
      throw new Error("Patient.getIsDeleted is neither string nor integer " + test.getClass());
    }

    return isDeleted.intValue() != 0;
  }

  /**
   * Checks if this entity is deleted.
   *
   * @return the boolean
   */
  public Boolean isDeleted() {
    return getIsDeleted();
  }

  /**
   * Marks all children of the given type as deleted.
   *
   * @param type the type
   * @return the boolean
   */
  public Boolean deleteChildren(String type, JSONResource myConfig) {
    for (Entity child : getChildren(type, myConfig)) {
      child.delete();
    }

    return true;
  }

  /**
   * Purges (hard delete) all children of the given type.
   *
   * @param type the type
   * @return the boolean
   */
  public Boolean purgeChildren(String type, JSONResource myConfig) {
    for (Entity child : getChildren(type, myConfig)) {
      child.purge();
      removeChild(type, child);
    }

    return true;
  }

  /**
   * Purges (hard delete) the entity.
   *
   * @return the boolean
   */
  public Boolean purge() {
    if (resourceUri == null) {
      return false;
    }

    if (resource == null) {
      reloadResource();
    }

    database.deleteResource(resource);

    return true;
  }

  /**
   * Calculates a hash code based on this resources backend URI.
   *
   * @return the int
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getResourceUri() == null) ? 0 : getResourceUri().hashCode());
    return result;
  }

  /**
   * Sets the last change value to the current user and the current time. TODO: Should go into
   * backend (versionizing info) which is planned for a future version
   *
   * @param memoryOnly Do not save it into the DB
   */
  public void storeLastChange(boolean... memoryOnly) {
    if (FacesContext.getCurrentInstance() == null) {
      Utils.getLogger().error("storeLastChange called without a faces context!");
      return;
    }

    Entity lastChangedBy =
        (Entity)
            ((AbstractSessionBean) Utils.getBackingBean("sessionBean")).getCurrentObject("user");
    if (lastChangedBy == null) {
      Utils.getLogger().error("storeLastChange called even though there is no current User!");
      return;
    }

    Date lastChangedTime = Calendar.getInstance().getTime();

    String lastChangedTimeString = DataConverter.convertDateToSecuredTimeString(lastChangedTime);

    properties.put(Attributes.lastChangedBy, lastChangedBy);
    properties.put(Attributes.lastChangedDate, lastChangedTime);

    if (memoryOnly.length == 0 || !memoryOnly[0]) {
      resource.setProperty(Attributes.lastChangedBy, lastChangedBy.getResource());
      resource.setProperty(Attributes.lastChangedDate, lastChangedTimeString);

      database.save(resource);
    }
  }

  //    private void loadLastChanged() {
  //        Utils.getLogger().warn("CALL WITHOUT CONFIG...HAVE TO RELOAD");
  //        loadLastChanged(database.getConfig("osse"));
  //    }

  /**
   * Gets the entity who changed this entity last.
   *
   * @return the last changed by
   */
  public Entity getLastChangedBy() {
    if (!properties.containsKey(Attributes.lastChangedBy)) {
      return null;
    }

    return (Entity) properties.get(Attributes.lastChangedBy);
  }

  /**
   * Gets the date this entity was last changed.
   *
   * @return the last changed date
   */
  public Date getLastChangedDate() {
    if (!properties.containsKey(Attributes.lastChangedDate)) {
      return null;
    }

    return (Date) properties.get(Attributes.lastChangedDate);
  }

  /**
   * Gets the last changed date as string.
   *
   * @return the last changed date as string
   */
  public String getLastChangedDateAsString() {
    if (!properties.containsKey(Attributes.lastChangedDate)) {
      return null;
    }

    Date lastChanged = (Date) properties.get(Attributes.lastChangedDate);
    if (lastChanged == null) {
      return null;
    }
    String dateTimeStringFormat = Utils.getResourceBundleString("date_time_format");
    return Utils.getDateString(lastChanged, dateTimeStringFormat);
  }

  /** Load last changed values. */
  private void loadLastChanged(JSONResource myConfig) {
    Value lastChanged = resource.getProperty(Attributes.lastChangedDate);
    if (lastChanged != null) {
      Date lastChangedDate = DataConverter.convertSecuredTimeStringToDate(lastChanged.getValue());
      properties.put(Attributes.lastChangedDate, lastChangedDate);
    }

    Value lastChangedBy = resource.getProperty(Attributes.lastChangedBy);
    if (lastChangedBy != null) {
      Resource lastChangedUserRes = database.getResourceByIdentifier(lastChangedBy.getValue());
      Entity u = new Entity(database, "OSSEVocabulary.Type.User", lastChangedUserRes);
      u.load(myConfig);
      properties.put(Attributes.lastChangedBy, u);
    }
  }

  /**
   * Gets the attribute.
   *
   * @param key the key
   * @return the attribute
   */
  @Deprecated
  public Object getAttribute(String key) {
    return properties.get(key);
  }

  /**
   * Gets the property.
   *
   * @param key the key
   * @return the property
   */
  public Object getProperty(String key) {
    return properties.get(key);
  }

  /**
   * Checks for attribute.
   *
   * @param key the key
   * @return the boolean
   */
  @Deprecated
  public Boolean hasAttribute(String key) {
    return properties.containsKey(key);
  }

  /**
   * Checks for property.
   *
   * @param key the key
   * @return the boolean
   */
  public Boolean hasProperty(String key) {
    return properties.containsKey(key);
  }

  /**
   * Removes all values of a property.
   *
   * @param key the key
   */
  public void removeProperty(String key) {
    if (properties.containsKey(key)) {
      properties.remove(key);

      addRemovedProperty(key);
    }
  }

  /**
   * Adds a property to be removed to our list.
   *
   * @param key the key
   */
  public void addRemovedProperty(String key) {
    if (removedProperties == null) {
      removedProperties = new ArrayList<>();
    }

    removedProperties.add(key);
  }

  /**
   * Adds the property.
   *
   * @param key the key
   * @param val the val
   */
  @SuppressWarnings("unchecked")
  public void addProperty(String key, Object val) {
    if (properties.containsKey(key)) {
      Object temp = properties.get(key);
      if (temp instanceof List) {
        ((List<Object>) temp).add(val);
      } else {
        List<Object> newList = new ArrayList<>();
        newList.add(temp);
        newList.add(val);
        properties.put(key, newList);
      }
    } else {
      properties.put(key, val);
    }
  }

  /**
   * Adds the attribute.
   *
   * @param key the key
   * @param val the val
   */
  @SuppressWarnings("unchecked")
  @Deprecated
  public void addAttribute(String key, Object val) {
    addProperty(key, val);
  }

  /**
   * Sets the property.
   *
   * @param key the key
   * @param val the val
   */
  public void setProperty(String key, Object val) {
    properties.put(key, val);
  }

  /**
   * Sets the attribute.
   *
   * @param key the key
   * @param val the val
   */
  @Deprecated
  public void setAttribute(String key, Object val) {
    properties.put(key, val);
  }

  /**
   * Gets the attributes.
   *
   * @return the attributes
   */
  @Deprecated
  public HashMap<String, Object> getAttributes() {
    return properties;
  }

  /**
   * Sets the attributes.
   *
   * @param attributes the attributes
   */
  @Deprecated
  public void setAttributes(HashMap<String, Object> attributes) {
    this.properties = attributes;
  }

  /**
   * Gets the properties.
   *
   * @return the properties
   */
  public HashMap<String, Object> getProperties() {
    return properties;
  }

  /**
   * Sets the properties.
   *
   * @param attributes the attributes
   */
  public void setProperties(HashMap<String, Object> attributes) {
    this.properties = attributes;
  }

  /**
   * Compares an entity with another based on their backend IDs.
   *
   * @param compareObject the compare object
   * @return 0 if this entity's resource is null; 0 if both entities' resources are equal; -1 if the
   *     argument's resource is null
   */
  @Override
  public int compareTo(Entity compareObject) {
    if (getResource() == null) {
      return 0;
    }

    if (compareObject.getResource() == null) {
      return -1;
    }

    return Integer.compare(getResource().getId(), compareObject.getResource().getId());
  }

  /**
   * Checks if one entity is equal to the other based on their backend URIs.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    if (getUri() == null) {
      return ((Entity) obj).getUri() == null;
    } else {
      return getUri().equals(((Entity) obj).getUri());
    }
  }

  //    public Entity getParent(String parentType) {
  //        Utils.getLogger().warn("CALL WITHOUT CONFIG...HAVE TO RELOAD");
  //        return getParent(parentType, database.getConfig("osse"));
  //    }

  /**
   * Gets the temporary identifier.
   *
   * @return the tmp id
   */
  public String getTmpID() {
    return tmpID;
  }

  /**
   * Sets the temporary identifier.
   *
   * @param tmpID the new tmp id
   */
  public void setTmpID(String tmpID) {
    this.tmpID = tmpID;
  }

  /**
   * Gets the backend OSSEVocabulary.Type.* of this entity
   *
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * Gets the parent of this entity of a certain OSSEVocabulary.Type.* type.
   *
   * @param parentType the parent type
   * @return the parent
   */
  public Entity getParent(String parentType, JSONResource myConfig) {
    if (parents.get(parentType) == null) {
      Entity e = null;

      ResourceQuery criteria = new ResourceQuery(parentType);
      criteria.add(Criteria.Equal(this.type, BasicDB.ID, this.getId()));
      ArrayList<Resource> theParents = getDatabase().getResources(criteria);
      for (Resource r : theParents) {
        e = getEntity(parentType, r.getValue());
        e.load(myConfig);
      }

      parents.put(parentType, e);
      return e;
    }

    return parents.get(parentType);
  }

  /**
   * Gets "the parent". Throws an error, in case an entity has several parents.
   *
   * @return the parent
   */
  public Entity getParent(JSONResource myConfig) {
    if (numberParents != 1) {
      throw new Error(
          "Entity.getParent() called in type="
              + this.type
              + " but that type has "
              + numberParents
              + " types of parent! You need to call getParent(parentType) instead!");
    }

    String parentType = getTheOneParentType();

    if (parentType == null) {
      throw new Error(
          "Entity.getParent type "
              + this.type
              + " apparantly has no parent but getParent() was called!");
    }

    return getParent(parentType, myConfig);
  }

  /**
   * Gets the "the one" parent OSSEVocabulary.Type.* type, which is simply the last we can find in
   * the list. Handle this with great care.
   *
   * @return the one parent type
   */
  private String getTheOneParentType() {
    // Find Parent Type
    List<String> parentTypes = DatabaseDefinitions.getParent(this.type);
    String parentType = null;
    for (String pt : parentTypes) {
      parentType = pt;
    }

    return parentType;
  }

  /**
   * Checks if is valid parent type.
   *
   * @param parentType the parent type
   * @return the boolean
   */
  private Boolean isValidParentType(String parentType) {
    List<String> parentTypes = DatabaseDefinitions.getParent(this.type);
    return parentTypes.contains(parentType);
  }

  /**
   * Sets the parent.
   *
   * @param parent the new parent
   */
  public void setParent(Entity parent) {
    if (!isValidParentType(parent.getType())) {
      throw new Error(
          "Entity.setParent() with an invalid parent type "
              + parent.getType()
              + " called in type="
              + this.type);
    }

    parents.put(parent.getType(), parent);
  }

  /**
   * Gets the children as hash map, keyed by the OSSEVocabulary.Type.* type
   *
   * @param type the OSSEVocabulary.Type.* type of the children to get
   * @return A HashMap of the children where keys are OSSEVocabulary.Type.* types.
   */
  public HashMap<String, Entity> getChildrenHash(String type, JSONResource myConfig) {
    if (children.get(type) == null) {
      loadDescendants(type, myConfig);
    }

    return children.get(type);
  }

  /**
   * Gets a list of children of a certain OSSEVocabulary.Type.* type
   *
   * @param type the OSSEVocabulary.Type.* type of the children to get
   * @return the children
   */
  public List<Entity> getChildren(String type, JSONResource myConfig) {
    return new ArrayList<>(getChildrenHash(type, myConfig).values());
  }

  /**
   * Clears the sorted children list of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the children list to clear
   */
  public void clearSortedChildren(String type) {
    if (sortedChildren == null) {
      return;
    }

    sortedChildren.remove(type);
  }

  /**
   * Gets the sorted children list of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the chldren to get
   * @return the sorted children
   */
  public List<Entity> getSortedChildren(String type, JSONResource myConfig) {
    return getSortedChildren(type, false, myConfig);
  }

  /**
   * Gets a sorted children list of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the children to get
   * @param reverse Whether to sort in reverse order
   * @return the sorted children list
   */
  public List<Entity> getSortedChildren(String type, Boolean reverse, JSONResource myConfig) {
    if (sortedChildren == null) {
      sortedChildren = new HashMap<>();
    }

    if (sortedChildren.get(type) == null) {
      List<Entity> temp = getChildren(type, myConfig);

      Collections.sort(temp);
      if (reverse) {
        Collections.reverse(temp);
      }

      sortedChildren.put(type, temp);
    }

    return sortedChildren.get(type);
  }

  /**
   * Checks if a OSSEVocabulary.Type.* type has a customized children hash primary key
   *
   * @param type the OSSEVocabulary.Type.* type
   * @return true, if successful
   */
  public boolean hasCustomizedChildrenHash(String type) {
    return childrenHashPrimaryKey.get(type) != null;
  }

  /**
   * Gets the children hash primary key of a certain OSSEVocabulary.Type.* type
   *
   * @param type the OSSEVocabulary.Type.* type
   * @return the children hash primary key
   */
  public String getChildrenHashPrimaryKey(String type) {
    return childrenHashPrimaryKey.get(type);
  }

  /**
   * Sets the children hash primary key of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @param primaryKey the primary key
   */
  public void setChildrenHashPrimaryKey(String type, String primaryKey) {
    this.childrenHashPrimaryKey.put(type, primaryKey);
  }

  /**
   * Has this entity any children of a certain OSSEVocabulary.Type.* type?
   *
   * @param type the OSSEVocabulary.Type.* type
   * @return true|false
   */
  public boolean hasChildren(String type, JSONResource myConfig) {
    return getChildrenHash(type, myConfig).size() > 0;
  }

  /**
   * Gets the OSSEVocabulary.Type.* types of children of this entity.
   *
   * @return the children types
   */
  public Set<String> getChildrenTypes() {
    return children.keySet();
  }

  /**
   * Finds the object of a child and replaces it with the new given child. It is found and replaced
   * by its URI or its primaryKey depending on how the children are organized.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @param child the child to set
   * @return Entity the entity that got replaced
   */
  public Entity replaceChild(String type, Entity child, JSONResource myConfig) {
    if (child == null) {
      return null;
    }

    Entity itisme;

    if (hasCustomizedChildrenHash(type)) {
      itisme =
          getChild(type, (String) child.getProperty(getChildrenHashPrimaryKey(type)), myConfig);
    } else {
      itisme = getChild(type, child.getUri(), myConfig);
    }

    if (itisme != null) {
      removeChild(type, itisme);
    }

    addChild(type, child);

    return itisme;
  }

  /**
   * Gets a certain child of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the child to get
   * @param key the key of the child to get
   * @return the child
   */
  public Entity getChild(String type, String key, JSONResource myConfig) {
    return getChildrenHash(type, myConfig).get(key);
  }

  /**
   * Checks for certain child of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the child to get
   * @param key the key of the child to get
   * @return true, if successful
   */
  public boolean hasChild(String type, String key, JSONResource myConfig) {
    return getChildrenHash(type, myConfig).containsKey(key);
  }

  /**
   * Checks for a child entity of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @param child the child
   * @return true, if such a child entity exists.
   */
  public boolean hasChild(String type, Entity child, JSONResource myConfig) {
    return getChildrenHash(type, myConfig).containsValue(child);
  }

  /**
   * Adds a child entity to the list of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the new child
   * @param child the child to be added
   */
  public void addChild(String type, Entity child) {
    if (child == null) {
      return;
    }

    if (!children.containsKey(type)) {
      children.put(type, new HashMap<String, Entity>());
    }

    if (hasCustomizedChildrenHash(type)) {
      children.get(type).put((String) child.getProperty(getChildrenHashPrimaryKey(type)), child);
    } else {
      children.get(type).put(child.getUri(), child);
    }

    clearSortedChildren(type);
  }

  //    public void loadDescendants(String type) {
  //        Utils.getLogger().warn("CALL WITHOUT CONFIG...HAVE TO RELOAD");
  //        loadDescendants(type, database.getConfig("osse"));
  //    }

  /**
   * Removes a given child of a certain OSSEVocabulary.Type.* type.
   *
   * @param type OSSEVocabulary.Type.* of the child to remove
   * @param child the child to remove
   */
  public void removeChild(String type, Entity child) {
    if (child == null) {
      return;
    }

    if (!children.containsKey(type)) {
      return;
    }

    if (hasCustomizedChildrenHash(type)) {
      children.get(type).remove(child.getProperty(getChildrenHashPrimaryKey(type)));
    } else {
      children.get(type).remove(child.getUri());
    }

    clearSortedChildren(type);
  }

  /**
   * Removes a child of a certain OSSEVocabulary.Type.* type by its key.
   *
   * @param type the OSSEVocabulary.Type.* type of the child to remove
   * @param key URI or primaryKey of the child to remove
   * @return the removed entity
   */
  public Entity removeChild(String type, String key, JSONResource myConfig) {
    Entity removeMe = getChild(type, key, myConfig);
    if (removeMe == null) {
      return null;
    }

    removeChild(type, removeMe);
    return removeMe;
  }

  /**
   * Clear all children of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the children to remove
   */
  public void clearChildren(String type) {
    if (children.get(type) == null) {
      return;
    }

    children.get(type).clear();
    clearSortedChildren(type);
  }

  /**
   * Load the descendants of a certain OSSEVocabulary.Type.* type. Descendants are children or
   * grandchildren (or beyond) of an entity So if you need a list of the grandchildren of an entity,
   * use this method
   *
   * @param type the OSSEVocabulary.Type.* type for which to get the descendents
   */
  public void loadDescendants(String type, JSONResource myConfig) {

    HashMap<String, String> childrenMap =
        (HashMap<String, String>) DatabaseDefinitions.getChildren(this.type);

    for (Entry<String, String> child : childrenMap.entrySet()) {
      if (child.getValue().equals(type)) {
        loadChildren(type, false, myConfig);
        break;
        // TODO: Aren't we already done here? Why do the rest below,
        // too?
      }
    }

    children.put(type, new HashMap<String, Entity>());
    clearSortedChildren(type);

    ResourceQuery criteria = new ResourceQuery(type);
    criteria.add(Criteria.Equal(this.type, BasicDB.ID, this.getId()));
    ArrayList<Resource> theDescendants = getDatabase().getResources(criteria);

    for (Resource r : theDescendants) {
      Entity e = getEntity(type, r.getValue());
      e.load(myConfig);

      if (!e.isDeleted()) {
        if (hasCustomizedChildrenHash(type)) {
          children.get(type).put((String) e.getProperty(getChildrenHashPrimaryKey(type)), e);
        } else {
          children.get(type).put(e.getUri(), e);
        }
      }
    }
  }

  /**
   * gets the configured value instance.project either from the applicationBean. If there is no
   * faces context it gets it directly from the configuration (less efficient)
   */
  private String getProjectInstanceName() {
    String projectInstanceName;
    if (FacesContext.getCurrentInstance() == null) {
      Configuration config = Utils.getConfig();
      projectInstanceName = config.getString("instance.project");
    } else {
      projectInstanceName = Utils.getAB().getConfig().getString("instance.project");
    }

    return projectInstanceName;
  }

  /**
   * Gets the initialized final class of an entity. As entity is a generic construct, we do not
   * really know which Java class contains an entity of a certain OSSEVocabulary.Type.* type. If you
   * have own classes that extend this, you'll have to put them into the package
   * "de.samply.edc.PROJECTNAME.model.XYZ", where PROJECTNAME stands for your project name and XYZ
   * for the class name.
   *
   * <p>This method finds the java class, and constructs it with correct casting.
   *
   * @param type the OSSEVocabulary.Type.* type of the entity class to get
   * @param uri the backend URI
   * @return the constructed entity
   */
  @SuppressWarnings("unchecked")
  public Entity getEntity(String type, String uri) {
    Class<? extends Entity> c = getTypeClass(type);

    Entity entity = null;

    if (c != null) {
      try {
        Constructor<? extends Entity> cons =
            c.getConstructor(de.samply.edc.control.AbstractDatabase.class, String.class);
        entity = cons.newInstance(database, uri);
      } catch (NoSuchMethodException
          | SecurityException
          | InstantiationException
          | IllegalAccessException
          | IllegalArgumentException
          | InvocationTargetException e) {
        Utils.getLogger().warn("Unable to get entity with type:" + type + " and uri:" + uri, e);
      }
    }

    return entity;
  }

  /**
   * Gets the initialized final class of an entity. As entity is a generic construct, we do not
   * really know which Java class contains an entity of a certain OSSEVocabulary.Type.* type. If you
   * have own classes that extend this, you'll have to put them into the package
   * "de.samply.edc.PROJECTNAME.model.XYZ", where PROJECTNAME stands for your project name and XYZ
   * for the class name.
   *
   * <p>This method finds the java class, and constructs it with correct casting.
   *
   * @param type the OSSEVocabulary.Type.* type of the entity class to get
   * @param resource the resource
   * @return the constructed entity
   */
  @SuppressWarnings("unchecked")
  public Entity getEntity(String type, Resource resource) {
    Class<? extends Entity> c = getTypeClass(type);

    Entity entity = null;

    if (c != null) {
      try {
        Constructor<? extends Entity> cons =
            c.getConstructor(de.samply.edc.control.AbstractDatabase.class, Resource.class);
        entity = cons.newInstance(database, resource);
      } catch (NoSuchMethodException
          | SecurityException
          | InstantiationException
          | IllegalAccessException
          | IllegalArgumentException
          | InvocationTargetException e) {
        Utils.getLogger()
            .warn("Unable to get entity with type:" + type + " and resource:" + resource.toString(),
                e);
      }
    }

    return entity;
  }

  //    public void loadChildren(String type, Boolean alsoLoadDeletedChildren) {
  //        Utils.getLogger().warn("CALL WITHOUT CONFIG...HAVE TO RELOAD");
  //        loadChildren(type, alsoLoadDeletedChildren, database.getConfig("osse"));
  //    }

  private Class<? extends Entity> getTypeClass(String type) {
    String upperCaseType;

    upperCaseType = Utils.upperCaseFirst(type);

    if (classes == null) {
      classes = new HashMap<String, Class<? extends Entity>>();
    }

    Class<? extends Entity> c = classes.get(type);

    if (c == null) {
      try {
        c =
            (Class<? extends Entity>)
                Class.forName(
                    "de.samply.edc." + getProjectInstanceName() + ".model." + upperCaseType);
        classes.put(type, c);
      } catch (ClassNotFoundException e) {
        try {
          c = (Class<? extends Entity>) Class.forName("de.samply.edc.model." + upperCaseType);
          classes.put(type, c);
        } catch (ClassNotFoundException e1) {
          try {
            // double fallback to original osse name
            c =
                (Class<? extends Entity>)
                    Class.forName("de.samply.edc.osse.model." + upperCaseType);
            classes.put(type, c);
          } catch (ClassNotFoundException e2) {
            Utils.getLogger()
                .error("Entity: Could not find any class with the name " + upperCaseType, e);
          }
        }
      }
    }
    return c;
  }

  /**
   * Load children of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type of the children to load
   * @param alsoLoadDeletedChildren if true, also deleted children will be loaded
   */
  public void loadChildren(String type, Boolean alsoLoadDeletedChildren, JSONResource myConfig) {
    loadChildren(type, alsoLoadDeletedChildren, myConfig, false);
  }

  /**
   * Load children of a certain OSSEVocatbulary.Type.* type.
   *
   * @param type the OSSEVocatbulary.Type.* type of the children to load
   * @param alsoLoadDeletedChildren if true, also deleted children will be loaded
   * @param forceLoadFromDb Whether to explicitly load the children information from the database.
   */
  public void loadChildren(
      String type,
      Boolean alsoLoadDeletedChildren,
      JSONResource myConfig,
      boolean forceLoadFromDb) {
    if (type == null) {
      return;
    }

    children.put(type, new HashMap<String, Entity>());
    clearSortedChildren(type);

    String childrenOnto = Utils.getPlural(type);

    reloadResource();

    // When it's a new entity (i.e. one that is not saved to the db yet),
    // there will not be a resource.
    // Since childrenURI would also be null in that case, we can just return
    // here.
    if (resource == null) {
      return;
    }

    Collection<Resource> childrenUris;

    if (forceLoadFromDb
        || resource.getProperty(childrenOnto) instanceof ResourceIdentifierLiteral) {
      childrenUris = resource.getResources(childrenOnto, database.getDatabaseModel());
    } else {
      childrenUris = new ArrayList<>();
      for (Value value : resource.getProperties(childrenOnto)) {
        if (value instanceof Resource) {
          childrenUris.add((Resource) value);
        }
      }
    }
    if (childrenUris != null && childrenUris.size() > 0) {
      for (Resource it : childrenUris) {
        Entity e = getEntity(type, it);

        e.setParent(this);
        e.load(myConfig);

        if (alsoLoadDeletedChildren || !e.isDeleted()) {
          if (hasCustomizedChildrenHash(type)) {
            if (children.get(type)
                .containsKey((String) e.getProperty(getChildrenHashPrimaryKey(type)))) {
              Utils.getLogger().warn(
                  "Duplicate Entity found for key " + e.getProperty(getChildrenHashPrimaryKey(type))
                      + " (" + children.get(type)
                      .get((String) e.getProperty(getChildrenHashPrimaryKey(type))).getResource()
                      .getValue() + " and " + e.getResource().getValue() + ")");
            }
            children.get(type).put((String) e.getProperty(getChildrenHashPrimaryKey(type)),
                e.getEntityChangedLast(children.get(type)
                    .get((String) e.getProperty(getChildrenHashPrimaryKey(type)))));
          } else {
            if (children.get(type).containsKey(e.getUri())) {
              Utils.getLogger().warn(
                  "Duplicate Entity found for key " + e.getUri() + " (" + children.get(type)
                      .get(e.getUri()).getResource().getValue() + " and " + e.getResource()
                      .getValue() + ")");
            }
            children.get(type)
                .put(e.getUri(), e.getEntityChangedLast(children.get(type).get(e.getUri())));
          }
        }
      }
    }
  }

  /**
   * Compares the property "lastChangeDate" of two entities and returns the one with the most recent
   * date.
   *
   * @param compareEntity Entity to compare this to.
   * @return The entity with the most recent lastChangeDate. this, if the argument is null. If one
   *     of the entities lastChangeDate is null, the one with a lastChangeDate is returned. If both
   *     entities' dates are null, e1 is returned. If both entities' dates are equal, e1 is
   *     returned.
   */
  public Entity getEntityChangedLast(Entity compareEntity) {
    if (compareEntity == null) {
      return this;
    }
    Date lastChangeDateE1 = getLastChangedDate();
    Date lastChangeDateE2 = compareEntity.getLastChangedDate();
    if (lastChangeDateE1 == null && lastChangeDateE2 != null) {
      return compareEntity;
    } else if (lastChangeDateE1 != null && lastChangeDateE2 == null) {
      return this;
    } else if (lastChangeDateE1 == null && lastChangeDateE2 == null) {
      Utils.getLogger().warn(
          "Both duplicate entities have no change date: " + getResource().getValue()
              + " and " + compareEntity.getResource().getValue());
      int compareProperties = Integer.compare(getProperties().size(),
          compareEntity.getProperties().size());
      if (compareProperties < 0) {
        Utils.getLogger().warn("Entity " + getResource().getValue() + " has less properties than "
            + compareEntity.getResource().getValue() + ". Choosing the latter.");
        return compareEntity;
      } else if (compareProperties > 0) {
        Utils.getLogger().warn("Entity " + getResource().getValue() + " has more properties than "
            + compareEntity.getResource().getValue() + ". Choosing the former.");
        return this;
      } else {
        Utils.getLogger().warn("Both duplicate entities have the same number of properties: "
            + getResource().getValue() + " and " + compareEntity.getResource().getValue()
            + ". Choosing the former.");
        return this;
      }
    } else if (lastChangeDateE1.compareTo(lastChangeDateE2) < 0) {
      Utils.getLogger().warn("Entity " + getResource().getValue() + " is older than entity "
          + compareEntity.getResource().getValue() + ". Choosing the latter.");
      return compareEntity;
    } else if (lastChangeDateE1.compareTo(lastChangeDateE2) > 0) {
      Utils.getLogger().warn("Entity " + getResource().getValue() + " is newer than entity "
          + compareEntity.getResource().getValue() + ". Choosing the former.");
      return this;
    } else {
      Utils.getLogger().warn(
          "Duplicate entities have the exact same change date. This is very unusual: "
              + getResource().getValue() + " and " + compareEntity.getResource().getValue()
              + ". Choosing the former.");
      return this;
    }
  }

  /**
   * Gets all unsaved children of a OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @return the unsaved children
   */
  public List<Entity> getUnsavedChildren(String type, JSONResource myConfig) {
    List<Entity> children = getChildren(type, myConfig);
    for (Iterator<Entity> it = children.iterator(); it.hasNext(); ) {
      Entity e = it.next();
      if (e.getResource() != null) {
        it.remove();
      }
    }
    return children;
  }

  /**
   * Removes all unsaved children of a OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   */
  public void removeUnsavedChildren(String type, JSONResource myConfig) {
    for (Iterator<Entity> it = getChildren(type, myConfig).iterator(); it.hasNext(); ) {
      Entity e = it.next();
      if (e.getResource() == null) {
        removeChild(type, e);
      }
    }
  }

  /**
   * Checks if an entity exists by a property and value definition, for example check if an entity
   * of a certain OSSEVocabulary.Name exists with the name "foobar"
   *
   * @param property The property to check
   * @param value The value of the property to check
   * @return the backend resource
   */
  public Resource entityExistsByProperty(String property, String value) {
    ResourceQuery query = new ResourceQuery(this.type);
    query.setFetchAdjacentResources(false);
    query.add(Criteria.Equal(this.type, property, value));
    ArrayList<Resource> found = database.getResources(query);

    if (found.size() > 0) {
      return found.get(0);
    } else {
      return null;
    }
  }

  /**
   * Gets the backend id.
   *
   * @return the id
   */
  public int getId() {
    if (resource == null) {
      // New Entities don't have resources
      return -1;
    }

    return resource.getId();
  }

  /**
   * Gets the backend URI.
   *
   * @return the uri
   */
  public String getUri() {
    return resourceUri;
  }

  /**
   * Gets the backend uri.
   *
   * @return the uri
   */
  public String getResourceUri() {
    return resourceUri;
  }

  /**
   * Gets the backend resource.
   *
   * @return the resource
   */
  public Resource getResource() {
    return resource;
  }

  /**
   * Sets the backend resource.
   *
   * @param resource the new resource
   */
  public void setResource(Resource resource) {
    this.resource = resource;
    if (resource == null) {
      resourceUri = null;
    } else {
      resourceUri = resource.getValue();
    }
  }

  /**
   * Gets the database wrapper.
   *
   * @return the database
   */
  public AbstractDatabase<?> getDatabase() {
    return database;
  }

  /**
   * Sets the database wrapper.
   *
   * @param db the new database
   */
  public void setDatabase(AbstractDatabase<?> db) {
    database = db;
  }

  /**
   * The sub list acts as a cache for a list of children, for example a filtered list.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @param children the children
   */
  public void setSubListChildren(String type, ArrayList<Entity> children) {
    if (subListOfChildren == null) {
      subListOfChildren = new HashMap<>();
    }

    subListOfChildren.put(type, children);
  }

  /**
   * Adds a child to the sub list of children of a certain OSSEVocabulary.Type.* type (for example
   * used for filtered lists)
   *
   * @param type the OSSEVocabulary.Type.* type
   * @param child the child
   */
  public void addSubListChild(String type, Entity child) {
    if (subListOfChildren == null) {
      subListOfChildren = new HashMap<>();
    }

    ArrayList<Entity> oldList = subListOfChildren.get(type);

    if (oldList == null) {
      oldList = new ArrayList<>();
    }

    oldList.add(child);
    subListOfChildren.put(type, oldList);
  }

  /**
   * Gets the sub list children of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   * @return the sub list children
   */
  public ArrayList<Entity> getSubListChildren(String type) {
    if (subListOfChildren == null) {
      subListOfChildren = new HashMap<>();
    }

    return subListOfChildren.get(type);
  }

  /**
   * Clear sub list children of a certain OSSEVocabulary.Type.* type.
   *
   * @param type the OSSEVocabulary.Type.* type
   */
  public void clearSubListChildren(String type) {
    if (subListOfChildren == null || subListOfChildren.get(type) == null) {
      return;
    }

    subListOfChildren.get(type).clear();
  }

  /**
   * Gets the sub list of children of all OSSEVocabulary.Type.* types.
   *
   * @return the sub list of children
   */
  public HashMap<String, ArrayList<Entity>> getSubListOfChildren() {
    return subListOfChildren;
  }

  /**
   * Sets the sub list of children of all OSSEVocabulary.Type.* types.
   *
   * @param subListOfChildren the sub list of children
   */
  public void setSubListOfChildren(HashMap<String, ArrayList<Entity>> subListOfChildren) {
    this.subListOfChildren = subListOfChildren;
  }

  /** This method forces a reload. */
  public void forceReload() {
    this.resource = null;
    reloadResource();
  }
}
